# Multifidelity monte carlo methods for extremes
A working repository for this project. Note this is not an attempt to make an R package.

You might find the little [Langevin diffusion simulation](https://gitlab.com/mfextreme/mf_boot/-/snippets/2065453), in snippets, useful for this project or other purposes.


## Development guide

### Adding likelihood functions
#### Basic
You must complete each step to make a given likelihood function available for the `mf_boot_mle` and related procedures.

If adding loglikelihoods is a feature you'd like to make even simpler, by reducing some repetition in the changes of step 2 below for example: That is possible but might require a fair bit of work.

1. Add the *negative* loglikelihood function to `R/ev_estimation.R` with naming convention `name_lln`. This name will also serve as the `lln_name` value for that function.
	- your function must take vector arguments named par and x, giving the parameter vector over which to optimize for MLE calculations and x the data
	- make a comment about the order and names for the par arguments. if possible it should follow the convention of existing lln functions.
2. Add a corresponding distribution function to `R/ev_estimation.R`. See `pgumbel, pgev` for conventions.
3. Include the appropriate behavior for your new `name_lln` in switch statements within the following functions:
	- in `mf_boot_core.R`:
		- `.optim_switch_uv`
		- `.boot_to_dt`  
	- in `mf_boot.R`
		- `mf_boot_mle_` for the line assigning to `checks`
		- `.mf_estimate_switch`
		- `marginal_estimate`

#### Explicit MLE
Adding functionality for a likelihood whose MLE is in closed form is an exercise in making sure the inputs and outputs are compatible with the existing inputs and outputs of `mf_boot_mle_` and the functions it wraps that you ultimately will use, `mf_boot_mle` and `Pmf_boot_mle`. This will ensure compatibility with the estimation functions, which plug the MLE into parametric models for exceedance probabilities.

1. Add the mle calculation in `ev_estimation.R` with a name that is roughly consistent with other syntax, e.g. `gaussian_mle`, or even `gaussian_lln` if the misnaming doesn't bother you and you want to be as consistent as possible with other syntax. This will also serve as the `lln_name` argument for the `switch`-type functionality in `mf_boot_mle_` and related functions.
2. Add a corresponding distribution function to `R/ev_estimation.R`
3. Include the appropriate behavior in the switch statements within the following functions:
	- in `mf_boot_core.R`
		- `.optim_switch_uv` add an arm to the switch statement that just computes the MLE instead of using optim. again, `lln_name` is a little bit of a misnomer for this case but use the *exact* mle function name as written in `ev_estimation.R`.
		- `.boot_to_dt` for correct parameter names
	- in `mf_boot.R`
		- in `mf_bool_mle_`
			- `checks`: the x0, lower, upper arguments are not needed so the all_names vector should be empty in that case. consider making c() (empty vector) the default for `mf_boot_mle_`
		- in `.mf_estimate_switch`  apply the appropriate cdf for the output from `mf_boot_mle` in this case
		- in `marginal_estimate`

**To keep in mind:** For top-level functions such as Pinsample_mf_boot, you could choose to pass the MLE-calculating function as the lln argument. Then the `lln_name` will be captured automatically and will give the correct value for the switch statements, assuming you've implemented everything correctly. If that bothered you, you could add an argument in Pinsample_mf_boot to pass the MLE-calculating function directly --- but then you would need to capture the name and set it to `lln_name` near the top Pinsample_mf_boot, which is more work.

#### Semi-explicit MLE
This is for cases where you need optimize only over a few parameters, then can compute the MLE for other parameters as a function of those.

The procedure here is a blend of the previous two cases, so I don't go over the details. The same functions need to be changed in the same places. The only wrinkle here is that the `lln` argument to MF boot will need to be the actual log-likelihood function (unlike in the explicit mle case), and you will need to call the MLE computation function inside the switch statement of `.optim_switch_uv` instead of passing it as an argument.

### Testing
There are some very simple tests in the `tests` folder. If the project grows it will be important to inlcude more and test at each change. See the comment below about packaging as well.

### Conventions

#### High-fidelity and low-fidelity

* In data inputs, which typically are data.table with two columns, the first column is the high-fidelity data and the second is low fidelity data. This is consistent throughout but is not easily changed.
* Parameter names in output follow the convention `param1` for the parameter `param` in the high-fidelity marginal and `param2` for the corresponding parameter in the low-fidelity marginal. For example, `sh1` refers to the high-fidelity shape parameter for the GEV model.


#### Function names

* Function names beginning with `.` are hidden objects, as in the unix-like convention.  As usual, such functions are not meant to be called by the user.
* Function names ending with `_`. This is somewhat vague, but such functions are ones that either are decorators or 'function factories.' In other words, they are functions that modify other functions to enforce certain consistent behavior. For example, `looper_` takes a function, augments its arguments with a `size` argument, and produces a function that repeats the original input function `size` number of times --- typically collecting its output in a data.table.
* Function names beginning with `P` are parallelized, using the `doParallel` or `doRNG` packages. It is important to keep track of where parallelization is happening because attempting to execute code in parallel at multiple levels can in fact slow things down.

### Make it a package?
If the plan is to continue developing this and potentially make it avaliable for others to use, it would a good idea to make this an R package with proper documentation, dependencies, test integration etc.
