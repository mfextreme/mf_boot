# TODO mf_boot_cluster is out of date with the rest of the usage in mf_boot, mf_boot_core etc and shouldn't be used till updated

# MULTIFIDELITY BOOTSTRAP ESTIMATION FOR EXTREMES
# version of procedure for running small, single-thread jobs
# with high io, run in parallel,
# for which cluster computing environments are optimized
# for testing on a home machine, see mf_boot.R for better performance
# separate file here since mf_boot.R will set multiple threads used in parallel

# TODO
# at present relies too much on top-level user to prevent data race conditions when running in parallel via job scheduler
# should instead write better top-level functions that prevent those things, for benefit of future self or other users

# TODO refactor for change to allow gev_lln not just gumbel_lln. breaking change for this procedure.

source("./R/mf_boot_core.R")

# THREADS FOR DATA.TABLE
# TODO Review later: Avoiding multilevel parallelization
# this is handled automatically when forked via foreach
# https://rdatatable.gitlab.io/data.table/reference/openmp-utils.html
# uncertain at the moment how this should be handled in cluster environment
# where each job might in fact have multiple threads available
# uncomment to test

# setDTthreads(1)


# MULTIFIDELITY ESTIMATION

# cluster implementation of mf_boot
# only difference is results are written out rather than returned

# TODO should wrap this in a function guaranteeing no data races.
#  flag new written to make multiple non-parallel runs possible, e.g. for adding data,
#  but it could be misused to cause a data race
Lmf_boot <- function(runid, file_root, d_joint, d_lofi, size, bsize, replace = T,
bv_ll = F, x0 = c(2, 2, 20, 10, 10), lower = c(0.1, 0.1, 1, -5, -5), upper =
c(50, 50, 100, 20, 20)){
  # TODO insert check or msg saying hifi must be first col of d_joint
  # for x0, lower, upper params passed to d_lofi estimate, assume lofi is second var in d_joint

  d_lofi <- as.data.table(d_lofi)
  d_joint <- as.data.table(d_joint)

  flag_new <- length(list.files(".", paste0(file_root, "_joint.csv"))) == 0

  # lofi-only mle estimate on d_lofi
  tryCatch({m_lofi <- optim(c(x0[2], x0[5]),
                        gumbel_lln, x = d_lofi[[1]],
                        method = "L-BFGS-B",
                        lower = c(lower[2], lower[5]), upper = c(upper[2], upper[5]))},
           error = function(e) {m_lofi <<- list(par = c(NA, NA))}
  )

  # make optional:
  # non-boot hifi-only mle for comparison
  tryCatch({m_hifi <- optim(c(x0[1], x0[4]),
                        gumbel_lln, x = d_joint[[1]],
                        method = "L-BFGS-B",
                        lower = c(lower[1], lower[4]), upper = c(upper[1], upper[4]))},
           error = function(e) {m_hifi <<- list(par = c(NA, NA))}
  )

  m_joint <- boot_mle(d_joint, size = size, bsize = bsize, replace = replace,
  bv_ll = bv_ll, x0 = x0, lower = lower, upper = upper)

  m_joint[, runid:=runid]

  m_lofi <- data.table(par = c("scale2", "loc2"), mle = m_lofi$par, runid =
  runid, row.names = NULL)

  m_hifi <- data.table(par = c("scale1", "loc1"), mle = m_hifi$par, runid =
  runid, row.names = NULL)

  fwrite(m_joint, paste0(file_root, "_joint.csv"), append = !flag_new)
  fwrite(m_lofi, paste0(file_root, "_lofi.csv"), append = !flag_new)
  fwrite(m_hifi, paste0(file_root, "_hifi.csv"), append = !flag_new)
}

# compute estimate for each bootstrap sample
# be aware of possible issues writing to common file for concurrent jobs
# at present just exceedance prob
# in future pass an estimation function with related args

Lmf_estimate <- function(xthresh, parid, file_in, file_out, first = F){
  # m must be '_joint.csv' output of Lprofile_mf_boot or Lmf_boot (one runid in latter)
  # parid is a generic label for a 'parameter set'

  m <- fread(file_in)

  m <- m[, .(p1 = 1-dgumbel(xthresh, .SD[par=="loc1", mle], .SD[par=="scale1",mle]),
  p2 = 1-dgumbel(xthresh, .SD[par=="loc2", mle], .SD[par=="scale2", mle])),
  by = .(bootid, runid)][, `:=`(diffp = p1 - p2, parid = parid)]

  fwrite(m, file_out, append = !first)
}



# PROFILING FUNCTIONS
# evaluate the mf_boot method for a range of dependencies
# in testing datasets

# parameter csv for iid draws from gumbel
Lbuild_partable <- function(file_name, res, scale = c(0.01, 10), s = c(1, 1e4), loc = c(-10, 10)){
  scale <- seq(scale[1], scale[2], by = res[1])
  s <- seq(s[1], s[2], by = res[2])
  loc <- seq(loc[1], loc[2], by = res[3])

  d <- CJ(scale1 = scale, scale2 = scale, s, loc1 = loc, loc2 = loc)

  fwrite(d, file_name)
}

.Lprofile_mf_boot <- function(runid, file_root, pars, n_lofi, n_joint, size,
bsize, replace = T, bv_ll = F, x0 = c(2, 2, 20, 10, 10), lower = c(0.1, 0.1, 1,
-20, -20), upper = c(50, 50, 100, 20, 20)){
  # generate lofi model: note total lofi samples is n_lofi+n_joint
  # still use joint: hifi coordinate will serve as test set

  n <- n_lofi + n_joint
  d <- as.data.table(rbvevd(n, dep=1/pars[3], model="log",
              mar1 = c(pars[4], pars[1], 0), mar2 = c(pars[5], pars[2], 0)))

  Lmf_boot(runid, file_root, d[1:n_joint], d[(n_joint + 1):n, 2],
           size = size, bsize = bsize, replace = replace,
           bv_ll = bv_ll, x0 = x0, lower = lower, upper = upper)

  # test set if needed
  flag_new_test <- length(list.files(".", paste0(file_root, "_testhifi.csv"))) == 0

  fwrite(d[(n_joint + 1):n, 1][, runid:=runid], paste0(file_root, "_testhifi.csv"), append = !flag_new_test)
}

# N runs of .Lprofile_mf_boot for a single set of pars
# this will correspond to one array job in cluster
# with array index running over a data frame with parameter values in rows
Lprofile_mf_boot <- function(N, file_root, pars, parid, n_lofi, n_joint, size, bsize,
replace = T, bv_ll = F, x0 = c(2, 2, 20, 10, 10), lower = c(0.1, 0.1, 1, -20,
-20), upper = c(50, 50, 100, 20, 20)){

  .Lprofile_mf_boot(1, paste0(file_root, "_", parid), pars,
  n_lofi = n_lofi, n_joint = n_joint,
  size = size, bsize = bsize, replace = replace,
  bv_ll = bv_ll, x0 = x0, lower = lower, upper = upper)

  for (i in 2:N){
    .Lprofile_mf_boot(i, paste0(file_root, "_", parid), pars, n_lofi, n_joint,
    size = size, bsize = bsize, replace = replace,
    bv_ll = bv_ll, x0 = x0, lower = lower, upper = upper)
  }
}
